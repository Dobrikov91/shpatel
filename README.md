# Shpatel

## Getting started

## Room

Room is a tool to create 3D space and control preson and speakers positions
You can use keyboard, OpetTack tools and SteamVR devices to control objects 

## usage

0. Navigate to Room folder
1. Create python virtualenv (python -m venv .venv)
2. Activate venv (e.g windows: .\.venv\Scripts\activate)
2. Install dependencies (pip install -r requirements.txt)
3. Run ZeroMQ forwarder in a separate shell (python .\services\zmq_forwarder.py)
4. Run main app (python .\main.py)
If you want to use VR headset add vr launch parameter (python .\main.py vr)

Keyboard commands are in trackers/keyboard.py

Services:
- Opentrack tools connection
To start opentrack server run it's module in a separate shell (python -m services.opentrack)
Then run any compatible app and use default network settings (localhost:4242)

## GoRe

GoRe plugin is a vst3 wrapper over GoogleResonance spaial audio library.

## build

0. Navigate to Plugins/GoRe folder
1. Download Google Resonance Audio library as a project submodule (git submodule update --init)
2. Use GitBash to download ResonanceAudio third-party dependencies (Plugins\GoRe\resonance-audio\third_party\clone_core_deps.sh)
3. Install vcpkg
https://learn.microsoft.com/en-us/vcpkg/get_started/get-started-msbuild?pivots=shell-cmd
4. Create VisualStudio project via Projucer

5. Setup VCPKG in project settings 
Configuration Properties => vcpkg =>
- Use VCPKG Manifest [Yes]
- Use static libraries [Yes]
- Set Vcpkg configuration [Debug] for project debug mode
for the following projects (SharedCode/VST3)

### REPEAT THIS STEP AFTER EACH Projucer project recreation

6. Build project
7. Load plugin to DAW/Juce AudioPluginHost
8. Choose config file (speakerConfigs.yml as example)
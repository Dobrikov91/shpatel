/*
  ==============================================================================

    ResonanceRenderer.h
    Created: 10 Feb 2024 10:08:11am
    Author:  dobri

  ==============================================================================
*/

#pragma once
#include <memory>
#include <vector>

#include "api/resonance_audio_api.h"

#include "ZmqServer.h"

class ResonanceRenderer
{
public:
	ResonanceRenderer(int numInputChannels, int sampleRate, int bufferSize);
	~ResonanceRenderer();

    void applyPos(std::vector<double> pos);
    void setSpeakerPos(std::vector<std::vector<double>> speakerPos);
    void render(const float* const* in, float* const* out);

private:
    size_t m_bufferSize;
    size_t m_sampleRate;

    std::unique_ptr<vraudio::ResonanceAudioApi> ra;
    std::vector<vraudio::ResonanceAudioApi::SourceId> sId;
    std::unique_ptr<ZmqServer> zmqServer;
};

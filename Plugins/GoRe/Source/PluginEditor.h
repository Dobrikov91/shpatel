/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "MainView.h"

//==============================================================================
/**
*/
class GoReAudioProcessorEditor  : public juce::AudioProcessorEditor
{
public:
    GoReAudioProcessorEditor (GoReAudioProcessor&);
    ~GoReAudioProcessorEditor() override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    GoReAudioProcessor& audioProcessor;
    std::unique_ptr<MainView> mainView;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GoReAudioProcessorEditor)
};

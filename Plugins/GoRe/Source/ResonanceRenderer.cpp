/*
  ==============================================================================

    ResonanceRenderer.cpp
    Created: 10 Feb 2024 10:08:11am
    Author:  dobri

  ==============================================================================
*/

#include "ResonanceRenderer.h"

ResonanceRenderer::ResonanceRenderer(int numInputChannels, int sampleRate, int bufferSize) :
    m_bufferSize(bufferSize),
    m_sampleRate(sampleRate),
    ra(vraudio::CreateResonanceAudioApi(2, m_bufferSize, m_sampleRate))
{
    for (int i = 0; i < numInputChannels; i++) {
        sId.push_back(ra->CreateSoundObjectSource(vraudio::kBinauralHighQuality));
    }

    zmqServer = std::make_unique<ZmqServer>();
    
    zmqServer->userCallback = [this](std::vector<double> pos) {
        applyPos(pos);
    };

    zmqServer->speakerPosCallback = [this](std::vector<std::vector<double>> pos) {
        setSpeakerPos(pos);
    };
}

ResonanceRenderer::~ResonanceRenderer()
{
}

void ResonanceRenderer::applyPos(std::vector<double> pos)
{
    ra->SetHeadPosition(pos[0], pos[1], pos[2]);
    // input quat   w, x, y, z
    // library quat x, y, z, w
    ra->SetHeadRotation(pos[4], pos[5], pos[6], pos[3]);
}

void ResonanceRenderer::setSpeakerPos(std::vector<std::vector<double>> speakerPos)
{
    for (int i = 0; i < speakerPos.size(); i++) {
        // drop extra speakers
        if (i >= sId.size()) {
            continue;
		}
		ra->SetSourcePosition(sId[i], speakerPos[i][0], speakerPos[i][1], speakerPos[i][2]);
	}
    // throw extra speakers to the center
    for (int i = speakerPos.size(); i < sId.size(); i++) {
		ra->SetSourcePosition(sId[i], 0, 0, 0);
	}
}

void ResonanceRenderer::render(const float* const* in, float* const* out)
{
    for (int i = 0; i < sId.size(); i++) {
        ra->SetPlanarBuffer(sId[i], in + i, 1, m_bufferSize);
    }
    ra->FillPlanarOutputBuffer(2, m_bufferSize, out);
}
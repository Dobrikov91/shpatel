/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
GoReAudioProcessorEditor::GoReAudioProcessorEditor (GoReAudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    mainView.reset(new MainView(p));
    addAndMakeVisible(mainView.get());
    
    setSize (600, 300);
}

GoReAudioProcessorEditor::~GoReAudioProcessorEditor()
{
}

//==============================================================================
void GoReAudioProcessorEditor::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));
}

void GoReAudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
    mainView->setBounds(getLocalBounds());
}

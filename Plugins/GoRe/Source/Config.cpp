/*
  ==============================================================================

    Config.cpp
    Created: 18 Feb 2024 2:14:03pm
    Author:  dobri

  ==============================================================================
*/

#include <cmath>
#include "Config.h"

#include "yaml-cpp/yaml.h"

Config::Config(std::string path)
{
	YAML::Node config = YAML::LoadFile(path);

    std::string current = config["current"].as<std::string>();
    double dist = config["dist"].as<double>();
	double height = config["height"].as<double>();

    for (auto setup : config["angles"])
	{
		std::string name = setup.first.as<std::string>();
		if (name != current)
			continue;
		this->name = name;
		for (auto speaker : setup.second)
		{
			this->speakerPos.push_back(angleToPos(speaker.as<double>(), dist, height));
		}
	}

	for (auto setup : config["pos"])
	{
		std::string name = setup.first.as<std::string>();
		if (name != current)
			continue;
		this->name = name;
		for (auto speaker : config["pos"][name])
		{
			std::vector<double> pos;
			for (auto coord : speaker.second)
			{
				pos.push_back(coord.as<double>());
			}
			this->speakerPos.push_back(pos);
		}
	}
}

std::vector<double> Config::angleToPos(double angle, double dist, double height)
{
	return {
		sin(angle * 3.1415926 / 180) * dist,
		cos(angle * 3.1415926 / 180) * dist,
		height
	};
}
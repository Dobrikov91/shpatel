/*
  ==============================================================================

    Config.h
    Created: 18 Feb 2024 2:14:03pm
    Author:  dobri

  ==============================================================================
*/

#pragma once
#include <string>
#include <vector>

class Config
{
public:
    Config(std::string path);

    std::vector<double> angleToPos(double angle, double dist, double height);

    std::string name;
    std::vector<std::vector<double>> speakerPos;
};
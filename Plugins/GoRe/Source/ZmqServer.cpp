/*
  ==============================================================================

    ZmqServer.cpp
    Created: 15 Mar 2024 4:53:38pm
    Author:  dobri

  ==============================================================================
*/

#include "ZmqServer.h"

ZmqServer::ZmqServer() :
    context(1)
{
    t = std::thread(&ZmqServer::subscriber_thread, this, std::ref(context));
}

ZmqServer::~ZmqServer() {
    this->stop();
}

void ZmqServer::subscriber_thread(zmq::context_t& ctx) {
    zmq::socket_t subscriber(ctx, ZMQ_SUB);
    subscriber.connect("tcp://127.0.0.1:4949");
    subscriber.set(zmq::sockopt::subscribe, "");

    while (!exitRequested) {
		zmq::message_t message;
		auto size = subscriber.recv(message, zmq::recv_flags::dontwait);

        if (size <= 0) {
			continue;
		}
        // stupid way to differentiate between user and speaker pos messages
        if (size == sizeof(double) * 7) {
            auto vals = parseDoublesFromBytes((const char*)message.data(), 7);
            if (userCallback) {
                userCallback(vals);
            }
        }
        else {
            auto vals = parseDoublesFromBytes((const char*)message.data(), message.size() / sizeof(double));
            std::vector<std::vector<double>> speakerPos;
            for (size_t i = 0; i < vals.size() / 3; i++) {
				speakerPos.push_back({ vals[i * 3], vals[i * 3 + 1], vals[i * 3 + 2] });
			}
            if (speakerPosCallback) {
				speakerPosCallback(speakerPos);
			}
        }
	}
}

void ZmqServer::stop() {
	exitRequested = true;
    t.join();
}

std::vector<double> ZmqServer::parseDoublesFromBytes(const char* bytes, size_t numDoubles) {
    std::vector<double> doubles(numDoubles);
    for (size_t i = 0; i < numDoubles; ++i) {
        std::memcpy(&doubles[i], bytes + i * sizeof(double), sizeof(double));
    }
    return doubles;
}
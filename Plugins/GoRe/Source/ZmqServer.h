/*
  ==============================================================================

    ZmqServer.h
    Created: 15 Mar 2024 4:53:38pm
    Author:  dobri

  ==============================================================================
*/

#pragma once

#include <functional>
#include <thread>

#include <zmq.hpp>


class ZmqServer {
public:
    ZmqServer();
    ~ZmqServer();

    void stop();

    std::function<void(std::vector<double>)> userCallback;
    std::function<void(std::vector<std::vector<double>>)> speakerPosCallback;

private:
    void subscriber_thread(zmq::context_t& ctx);
    std::vector<double> parseDoublesFromBytes(const char* bytes, size_t numDoubles);

    zmq::context_t context;
    std::thread t;

    bool exitRequested = false;
};
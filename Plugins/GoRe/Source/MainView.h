/*
  ==============================================================================

    MainView.h
    Created: 1 Apr 2024 2:53:57pm
    Author:  dobri

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "ResonanceRenderer.h"

class MainView : public juce::Component
{
public:
	MainView(GoReAudioProcessor& audioProcessor);
	~MainView() override;

	void paint(juce::Graphics& g) override;
	void resized() override;
private:
	void loadConfig();

	GoReAudioProcessor& audioProcessor;

	std::unique_ptr<juce::FileChooser> fileChooser;
	std::unique_ptr<juce::TextButton> openButton;
	std::unique_ptr<juce::Label> fileLabel;
};
/*
  ==============================================================================

    MainView.cpp
    Created: 1 Apr 2024 2:53:57pm
    Author:  dobri

  ==============================================================================
*/

#include "MainView.h"
#include "Config.h"

MainView::MainView(GoReAudioProcessor& audioProcessor) :
	audioProcessor(audioProcessor)
{
    fileChooser.reset(new juce::FileChooser(
        "Choose a config...", 
        juce::File::getSpecialLocation(juce::File::userHomeDirectory), 
        "*.yml", true, false, this));

    openButton.reset(new juce::TextButton("Open config"));
	
	fileLabel.reset(new juce::Label("File label", "No file selected"));
	openButton->onClick = [this] { loadConfig(); };

	addAndMakeVisible(openButton.get());
	addAndMakeVisible(fileLabel.get());
}

MainView::~MainView()
{
}

void MainView::paint(juce::Graphics& g)
{
}

void MainView::resized()
{
	auto area = getLocalBounds();
	auto buttonArea = area.removeFromTop(30);
	auto labelArea = area.removeFromTop(30);

	openButton->setBounds(buttonArea);
	fileLabel->setBounds(labelArea);
}

void MainView::loadConfig()
{
	fileChooser->launchAsync(juce::FileBrowserComponent::openMode | juce::FileBrowserComponent::canSelectFiles,
		[this](const juce::FileChooser& fc)
		{
			auto file = fc.getResult();
			if (file.exists())
			{
				Config c(file.getFullPathName().toStdString());
				if (audioProcessor.rr)
				{
					audioProcessor.rr->setSpeakerPos(c.speakerPos);
					fileLabel->setText(
						"Config " + c.name + ", " + file.getFullPathName().toStdString(), 
						juce::NotificationType::dontSendNotification);
				}
			}
		});
}
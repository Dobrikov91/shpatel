import yaml
import math

def anglesToPos(angles, dist, height):
    res = []
    for angle in angles:
        pos = [
            math.sin(angle * math.pi / 180) * dist,
            math.cos(angle * math.pi / 180) * dist,
            height
        ]
        res.append(pos)
    return res

def getConfig(yaml_file):
    with open(yaml_file, 'r') as file:
        config = yaml.safe_load(file)

    dist = config['dist']
    height = config['height']

    speakerSetupsPos = {}
    for setup in config['angles']:
        positions = anglesToPos(config['angles'][setup], dist, height)
        speakerSetupsPos[setup] = positions

    for setup in config['pos']:
        positions = []
        for pos in config['pos'][setup]:
            pos = config['pos'][setup][pos]
            positions.append([pos[0], pos[1], pos[2]])
        speakerSetupsPos[setup] = positions

    return speakerSetupsPos[config['current']]

if __name__ == '__main__':
    posConfigs = getConfig('speakerSetups.yml')
    print(posConfigs)

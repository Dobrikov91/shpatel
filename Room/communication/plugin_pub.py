import zmq
import struct

# refactor to reuse zmq_pub
class PluginPub():
    def __init__(self, port=4949) -> None:
        context = zmq.Context()
        self.socket = context.socket(zmq.PUB)
        addr = f"tcp://127.0.0.1:{port}"
        print(addr)
        self.socket.bind(addr)

    def sendPersonPos(self, pos, quat):
        message = struct.pack('ddddddd', *pos, *quat)
        self.socket.send(message)

    def sendSpeakerPos(self, pos):
        message = struct.pack('d'*len(pos), *pos)
        self.socket.send(message)
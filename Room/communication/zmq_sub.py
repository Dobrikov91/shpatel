import zmq
import struct
from threading import Thread

class ZmqSub:
    def __init__(self, topic, callback) -> None:
        self.topic = topic
        self.running = True
        self.callback = callback
        Thread(target=self.subscriberThread).start()

    def stop(self):
        self.running = False

    def subscriberThread(self):
        ctx = zmq.Context.instance()

        subscriber = ctx.socket(zmq.SUB)
        subscriber.connect("tcp://127.0.0.1:4546")
        subscriber.setsockopt(zmq.SUBSCRIBE, str.encode(self.topic))

        while self.running:
            try:
                event = subscriber.poll(timeout=100)
                if event == 0:
                    # timeout reached before any events were queued
                    pass
                else:
                    # events queued within our time limit
                    msg = subscriber.recv(flags=zmq.NOBLOCK)
                    if self.callback:
                        self.callback(self.unpackMessage(msg))
            except zmq.ZMQError as e:
                if e.errno == zmq.ETERM:
                    break
                else:
                    raise

    def unpackMessage(self, message):
        # cut off the topic
        messageData = message[8:]
        data = struct.unpack(len(messageData) // 8 * 'd', messageData)
        return data
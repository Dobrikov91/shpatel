import socket
from threading import Thread
import struct

'''
Server for OpenTrack tracker tools (e.g. AITrack https://github.com/AIRLegend/aitrack)
'''
class OtServer():
    def __init__(self, port=4242, callback=None):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.server_socket.settimeout(0.1)
        self.server_socket.bind(('', port))

        self.callback = callback

    def run(self):
        t = Thread(target=self.serv)
        self.running = True
        t.start()

    def stop(self):
        self.running = False
        self.server_socket.close()

    def serv(self):
        while self.running:
            try:
                message, address = self.server_socket.recvfrom(1024)
            except OSError:
                # print('Timeout')
                continue
            data = struct.unpack(6*'d', message)
            if self.callback:
                self.callback(data)


if __name__ == '__main__':
    s = OtServer()
    s.callback = print
    try:
        s.run()
        print('Run')
        while True:
            pass
    except:
        s.stop()
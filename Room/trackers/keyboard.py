from communication.zmq_pub import ZmqPub
from panda3d.core import LPoint3, Quat
from objects.person import Person

class Keyboard:
    def __init__(self, game, person: Person) -> None:
        self.person = person
        self.zmqPub = ZmqPub('P')

        # x y z
        self.pos = LPoint3(0, 0, 0)
        # yaw pitch roll
        self.rot = LPoint3(0, 0, 0)
        self.quat = Quat()

        self.actions = [
            # move
            ['arrow_left', self.stepMove, [-0.1, 0, 0]],
            ['arrow_right', self.stepMove, [0.1, 0, 0]],
            ['arrow_up', self.stepMove, [0, 0.1, 0]],
            ['arrow_down', self.stepMove, [0, -0.1, 0]],
            
            [',', self.stepMove, [0, 0, 0.1]],
            ['.', self.stepMove, [0, 0, -0.1]],

            # rotate
            ['a', self.stepRot, [10, 0, 0]], # left
            ['d', self.stepRot, [-10, 0, 0]], # right
            ['s', self.stepRot, [0, -10, 0]], # down
            ['w', self.stepRot, [0, 10, 0]], # up
            ['q', self.stepRot, [0, 0, -10]], # roll left
            ['e', self.stepRot, [0, 0, 10]], # roll right

            ['r', self.person.movable.recenter, []],

            ['0', self.changeObject, ['0']],
            ['1', self.changeObject, ['1']],
            ['2', self.changeObject, ['2']],
            ['3', self.changeObject, ['3']],
            ['4', self.changeObject, ['4']],
            ['5', self.changeObject, ['5']],
            ['6', self.changeObject, ['6']],
            ['7', self.changeObject, ['7']],
            ['8', self.changeObject, ['8']],
            ['p', self.changeObject, ['P']],
        ]

        for action in self.actions:
            game.accept(action[0], action[1], action[2])
            game.accept(action[0]+'-repeat', action[1], action[2])

    def sendInternalPos(self):
        self.zmqPub.sendDoubles([*self.pos, *self.quat])

    def stepMove(self, x, y, z):
        self.pos += LPoint3(x, y, z)
        self.sendInternalPos()

    def stepRot(self, yaw, pitch, roll):
        self.rot += LPoint3(yaw, pitch, roll)
        self.quat.set_hpr(self.rot)
        self.sendInternalPos()

    def resetPos(self):
        self.pos = LPoint3(0, 0, 0)
        self.rot = LPoint3(0, 0, 0)
        self.quat.set_hpr(self.rot)
        self.sendInternalPos()

    def changeObject(self, topic):
        self.zmqPub = ZmqPub(topic)
        self.resetPos()

import math
from communication.zmq_pub import ZmqPub
from panda3d.core import LPoint3, Quat

class MetaQuest:
    def __init__(self, topic):
        self.zmqPub = ZmqPub(topic)

    # https://github.com/TriadSemi/triad_openvr/blob/master/triad_openvr.py
    #Convert the standard 3x4 position/rotation matrix to a x,y,z location and the appropriate Quaternion
    def convert_to_quaternion(self, pose_mat):
        # Per issue #2, adding a abs() so that sqrt only results in real numbers
        r_w = math.sqrt(abs(1+pose_mat[0][0]+pose_mat[1][1]+pose_mat[2][2]))/2
        r_x = (pose_mat[2][1]-pose_mat[1][2])/(4*r_w)
        r_y = (pose_mat[0][2]-pose_mat[2][0])/(4*r_w)
        r_z = (pose_mat[1][0]-pose_mat[0][1])/(4*r_w)

        x = pose_mat[0][3]
        y = pose_mat[1][3]
        z = pose_mat[2][3]
        return [x,y,z,r_w,r_x,r_y,r_z]
    
    def convert(self, mat):
        vrpos = self.convert_to_quaternion(mat)

        # x z y
        pos = LPoint3(vrpos[0], -vrpos[2], vrpos[1])
        quat = Quat(vrpos[6], -vrpos[5], vrpos[3], vrpos[4]) # almost working
        # silly hack
        rot = quat.getHpr()
        rot[2] += 180
        quat.setHpr(rot)

        return [*pos, *quat]

    def publish(self, mat):
        self.zmqPub.sendDoubles(self.convert(mat))
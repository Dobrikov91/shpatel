from communication.ot_server import OtServer
from communication.zmq_pub import ZmqPub
from panda3d.core import LPoint3, Quat

class Opentrack:
    def __init__(self) -> None:
        self.udpClient = OtServer(4242, self.publish)
        self.zmqPub = ZmqPub("P")
        self.udpClient.run()

    def stop(self):
        self.udpClient.stop()

    # x y z yaw pitch roll
    # pos [x, y, z] (left-right, up-down, forward-backward) in cm
    # rot [yaw, pitch, roll] (0, 0, 90)
    def convert(self, data):
        pos = LPoint3(-data[0], data[1], data[2])
        pos /= 100

        rot = LPoint3(-1 * data[3], -1 * data[4], -1 * data[5] + 90)
        print(pos, rot)
        
        q = Quat()
        q.set_hpr(rot)

        return [*pos, *q]

    def publish(self, data):
        self.zmqPub.sendDoubles(self.convert(data))

if __name__ == '__main__':
    from time import sleep

    o = Opentrack()
    while True:
        try:
            sleep(1)
        except KeyboardInterrupt:
            break
    o.stop()
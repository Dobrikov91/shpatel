import zmq

# https://learning-0mq-with-pyzmq.readthedocs.io/en/latest/pyzmq/devices/forwarder.html
def main():
    try:
        context = zmq.Context.instance()
        # Socket facing clients
        frontend = context.socket(zmq.SUB)
        frontend.bind("tcp://127.0.0.1:4545")
        
        frontend.setsockopt(zmq.SUBSCRIBE, b"")
        
        # Socket facing services
        backend = context.socket(zmq.PUB)
        backend.bind("tcp://127.0.0.1:4546")

        zmq.device(zmq.FORWARDER, frontend, backend)
    except Exception as e:
        print(e)
        print("bringing down zmq device")
    finally:
        frontend.close()
        backend.close()
        context.term()

if __name__ == "__main__":
    main()
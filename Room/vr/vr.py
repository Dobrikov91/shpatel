from panda3d.core import ExecutionEnvironment
from p3dopenvr.hand import LeftHand, RightHand
from direct.task.TaskManagerGlobal import taskMgr
import os
from trackers.meta_quest import MetaQuest

import openvr

class VR:
    classes_map = { openvr.TrackedDeviceClass_Invalid: 'Invalid',
                    openvr.TrackedDeviceClass_HMD: 'HMD',
                    openvr.TrackedDeviceClass_Controller: 'Controller',
                    openvr.TrackedDeviceClass_GenericTracker: 'Generic tracker',
                    openvr.TrackedDeviceClass_TrackingReference: 'Tracking reference',
                    openvr.TrackedDeviceClass_DisplayRedirect: 'Display redirect',
                  }

    roles_map = { openvr.TrackedControllerRole_Invalid: 'Invalid',
                  openvr.TrackedControllerRole_LeftHand: 'Left',
                  openvr.TrackedControllerRole_RightHand: 'Right',
                  openvr.TrackedControllerRole_OptOut: 'Opt out',
                  openvr.TrackedControllerRole_Treadmill: 'Treadmill',
                  openvr.TrackedControllerRole_Stylus: 'Stylus',
                }

    buttons_map = { openvr.k_EButton_System: 'System',
                    openvr.k_EButton_ApplicationMenu: 'Application Menu',
                    openvr.k_EButton_Grip: 'Grip',
                    openvr.k_EButton_DPad_Left: 'Pad left',
                    openvr.k_EButton_DPad_Up: 'Pad up',
                    openvr.k_EButton_DPad_Right: 'Pad right',
                    openvr.k_EButton_DPad_Down: 'Pad down',
                    openvr.k_EButton_A: 'A',
                    openvr.k_EButton_ProximitySensor: 'Proximity sensor',
                    openvr.k_EButton_Axis0: 'Axis 0',
                    openvr.k_EButton_Axis1: 'Axis 1',
                    openvr.k_EButton_Axis2: 'Axis 2',
                    openvr.k_EButton_Axis3: 'Axis 3',
                    openvr.k_EButton_Axis4: 'Axis 4',
                    #openvr.k_EButton_SteamVR_Touchpad: 'Touchpad',
                    #openvr.k_EButton_SteamVR_Trigger: 'Trigger',
                    #openvr.k_EButton_Dashboard_Back: 'Dashboard back',
                    #openvr.k_EButton_IndexController_A: 'Controller A',
                    #openvr.k_EButton_IndexController_B: 'Controller B',
                    #openvr.k_EButton_IndexController_JoyStick: 'Controller joystick',
                    }

    button_events_map = { openvr.VREvent_ButtonPress: 'Press',
                            openvr.VREvent_ButtonUnpress: 'Unpress',
                            openvr.VREvent_ButtonTouch: 'Touch',
                            openvr.VREvent_ButtonUntouch: 'Untouch'
                        }

    def __init__(self, ovr, app):
        self.ovr = ovr
        self.app = app

        # Setup the application manifest, it will identify and configure the app
        # We force it in case it has changed.
        main_dir = ExecutionEnvironment.getEnvironmentVariable("MAIN_DIR")
        ovr.identify_application(os.path.join(main_dir, "vr", "actions.vrmanifest"), "p3dopenvr.vr.actions", force=True)

        # Load the actions manifest, it must be the same as the manifest referenced in the application manifest
        ovr.load_action_manifest(os.path.join(main_dir, "./vr/manifest/actions.json"))

        # Use the '/actions/default' action set. This action set will be updated each frame
        ovr.add_action_set("/actions/default")

        # Get the handle of the action '/actions/default/out/Haptic'. This hande will be used to trigger the haptic vibration.
        self.action_haptic = ovr.vr_input.getActionHandle('/actions/default/out/Haptic')

        # Get the handle of the action '/actions/default/in/Pose'. This hande will be used to update the position of the hands.
        hands_pose = ovr.vr_input.getActionHandle('/actions/default/in/Pose')

        # Get the handle of the action '/actions/default/in/GrabGrip'. This hande will be used to retrieve the data of the action.
        self.action_grip = ovr.vr_input.getActionHandle('/actions/default/in/GrabGrip')
        self.action_pinch = ovr.vr_input.getActionHandle('/actions/default/in/GrabPinch')

        # Create the representation of the left hand and attach a simple box on it
        self.left_hand = LeftHand(ovr, "box", hands_pose)
        self.left_hand.model.set_scale(0.05)

        # Create the representation of the right hand and attach a simple box on it
        self.right_hand = RightHand(ovr, "box", hands_pose)
        self.right_hand.model.set_scale(0.05)

        # Register the update task with the correct sort number
        taskMgr.add(self.update, sort=ovr.get_update_task_sort())

        self.person = MetaQuest("P")

        self.leftState = False
        self.rightState = False

        self.leftHand = None #MetaQuest("0")
        self.rightHand = None #MetaQuest("1")

    def update(self, task):
        # Retrieve the state of the Grip action and the device that has triggered it
        grip_state, device = self.ovr.get_digital_action_rising_edge(self.action_grip, device_path=True)
        pinch_state, device_pinch = self.ovr.get_digital_action_rising_edge(self.action_pinch, device_path=True)
        
        if grip_state:
            if device % 2 == 1:
                self.leftState = not self.leftState
                if self.leftState:
                    self.leftHand = MetaQuest("0")
                else:
                    self.leftHand = None
            else:
                self.rightState = not self.rightState
                if self.rightState:
                    self.rightHand = MetaQuest("1")
                else:
                    self.rightHand = None
            # If the grip is active, activate the haptic vibration on the same device
            self.ovr.vr_input.triggerHapticVibrationAction(self.action_haptic, 0, 1, 4, 1, device)
            print(device, grip_state)

        if pinch_state:
            print(device_pinch, pinch_state)

        # Update the position and orientation of the hands
        self.left_hand.update()
        self.right_hand.update()

        mat = self.ovr.poses[0].mDeviceToAbsoluteTracking
        self.person.publish(mat)

        mat = self.ovr.poses[1].mDeviceToAbsoluteTracking
        if self.leftHand is not None:
            self.leftHand.publish(mat)

        mat = self.ovr.poses[2].mDeviceToAbsoluteTracking
        if self.rightHand is not None:
            self.rightHand.publish(mat)

        self.app.sendAllPos()

        return task.cont
    
    def button_event(self, event):
        """
        Print the information related to the button event received.
        """

        device_index = event.trackedDeviceIndex
        device_class = self.ovr.vr_system.getTrackedDeviceClass(device_index)
        if device_class != openvr.TrackedDeviceClass_Controller:
            return
        button_id = event.data.controller.button
        button_name = self.buttons_map.get(button_id)
        if button_name is None:
            button_name = 'Unknown button ({})'.format(button_id)
        role = self.ovr.vr_system.getControllerRoleForTrackedDeviceIndex(device_index)
        role_name = self.roles_map.get(role)
        if role_name is None:
            role_name = 'Unknown role ({})'.format(role)
        event_name = self.button_events_map.get(event.eventType)
        if event_name is None:
            event_name = 'Unknown event ({})'.format(event.eventType)
        print(role_name, button_name, event_name)

    def device_event(self, event, action):
        """
        Print the information related to the device event received.
        """

        device_index = event.trackedDeviceIndex
        device_class = self.ovr.vr_system.getTrackedDeviceClass(device_index)
        class_name = self.classes_map.get(device_class)
        if class_name is None:
            class_name = 'Unknown class ({})'.format(class_name)
        print('Device {} {} ({})'.format(event.trackedDeviceIndex, action, class_name))

    def process_vr_event(self, event):
        print('!!!!', event.trackedDeviceIndex, event.eventType)
        if event.eventType == openvr.VREvent_TrackedDeviceActivated:
            self.device_event(event, 'attached')
        if event.eventType == openvr.VREvent_TrackedDeviceDeactivated:
            self.device_event(event, 'deactivated')
        elif event.eventType == openvr.VREvent_TrackedDeviceUpdated:
            self.device_event(event, 'updated')
        elif event.eventType in (openvr.VREvent_ButtonPress,
                                 openvr.VREvent_ButtonUnpress,
                                 openvr.VREvent_ButtonTouch,
                                 openvr.VREvent_ButtonUntouch):
            self.button_event(event)

from communication.zmq_sub import ZmqSub
from panda3d.core import LPoint3

class Movable():
    def __init__(self, object, topic) -> None:
        self.object = object
        self.sub = ZmqSub(topic, self.setPos)

        # todo add rot recenter
        self.posCenter = LPoint3(0, 0, 0)

    # 7 doubles: x, y, z, i, j, k, r
    def setPos(self, pos):
        xyz = LPoint3(*pos[:3])
        self.object.setPos(
            xyz - self.posCenter
        )
        self.object.setQuat(pos[3:])

    def recenter(self):
        self.posCenter = self.object.getPos() + self.posCenter
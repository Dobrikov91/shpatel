from objects.movable import Movable
from geom_models.card import Card
from geom_models.cube import Cube

speakerColors = [
    [1, 0, 0, 0.5],
    [0, 1, 0, 0.5],
    [0, 0, 1, 0.5],
    [1, 1, 0, 0.5],
    [1, 0, 1, 0.5],
    [0, 1, 1, 0.5],
    [1, 1, 1, 0.5],
    
    [0.5, 0, 0, 0.5],
    [0, 0.5, 0, 0.5],
    [0, 0, 0.5, 0.5],
    [0.5, 0.5, 0, 0.5],
    [0.5, 0, 0.5, 0.5],
    [0, 0.5, 0.5, 0.5],
    [0.5, 0.5, 0.5, 0.5],
]

class Speaker():
    def __init__(self, id: str, pos, renderer):
        self.obj = Cube(0.1)
        self.movable = Movable(self.obj, id)

        self.obj.set_pos(pos[0], pos[1], pos[2])
        self.obj.set_color(*speakerColors[int(id) % len(speakerColors)])
        self.obj.reparentTo(renderer)
        
        self.label = Card(id)
        self.label.reparentTo(self.obj)

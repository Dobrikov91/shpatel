from objects.movable import Movable
from geom_models.tetrahedron import Tetrahedron

class Person:
    def __init__(self, renderer) -> None:
        self.obj = Tetrahedron(0.3)
        self.movable = Movable(self.obj, "P")
        self.obj.reparentTo(renderer)

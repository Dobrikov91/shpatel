from direct.showbase.ShowBase import ShowBase
from panda3d.core import AntialiasAttrib
import sys
from objects.person import Person
from readConfig import getConfig
from objects.speaker import Speaker
import numpy as np
from communication.plugin_pub import PluginPub
from panda3d.core import ClockObject

from trackers.keyboard import Keyboard
from p3dopenvr.p3dopenvr import P3DOpenVR
from vr.vr import VR

from direct.task.TaskManagerGlobal import taskMgr
from geom_models.coordinate_system import drawCoordinateSystem

class Room(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        self.render.setAntialias(AntialiasAttrib.MAuto)

        self.accept("escape", self.exit)
        self.pluginPub = PluginPub(4949)

        drawCoordinateSystem(3, 0.5, self.render)

        self.person = Person(self.render)
        
        speakerPos = getConfig('../speakerConfigs.yml')

        self.speakers = []
        for pos in speakerPos:
            print(pos)
            self.speakers.append(Speaker(str(len(self.speakers)), pos, self.render))

        self.keyboard = Keyboard(self, self.person)
        taskMgr.add(self.sendAllPos, "sendAllPos")

    def sendAllPos(self, task=None):
        self.pluginPub.sendPersonPos(
            self.person.obj.getPos(), 
            self.person.obj.getQuat()
        )

        pos = []
        for speaker in self.speakers:
            pos.append(speaker.obj.getPos())
        self.pluginPub.sendSpeakerPos(np.array(pos).ravel())
        
        if task is not None:
            return task.cont

    def exit(self):
        self.person.movable.sub.stop()
        for speaker in self.speakers:
            speaker.movable.sub.stop()
        sys.exit()

try:
    app = Room()
    app.setFrameRateMeter(True)
    globalClock.setMode(ClockObject.MLimited)
    globalClock.setFrameRate(120)

    if len(sys.argv) > 1 and sys.argv[1] == 'vr':
        ovr = P3DOpenVR()
        ovr.init()
        demo = VR(ovr, app)
    app.run()
except:
    pass

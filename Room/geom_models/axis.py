from panda3d.core import NodePath, LineSegs

class Axis(NodePath):
    def __init__(self, size) -> None:
        ls = LineSegs()
        ls.set_thickness(3)
        ls.set_color(1.0, 0.0, 0.0, 1.0)
        
        # x
        ls.move_to(-size, 0, 0)
        ls.draw_to(size, 0, 0)
        # y
        ls.move_to(0, -size, 0)
        ls.draw_to(0, size, 0)
        # z
        ls.move_to(0, 0, -size)
        ls.draw_to(0, 0, size)

        NodePath.__init__(self, ls.create())

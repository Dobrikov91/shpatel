from panda3d.core import NodePath, LineSegs

class Grid(NodePath):
    def __init__(self, size, spacing) -> None:
        ls = LineSegs()

        # x
        i = -size
        while i < size:
            ls.move_to(i, -size, 0)
            ls.draw_to(i, size, 0)
            i += spacing
        # Y
        i = -size
        while i < size:
            ls.move_to(-size, i, 0)
            ls.draw_to(size, i, 0)
            i += spacing
            
        NodePath.__init__(self, ls.create())

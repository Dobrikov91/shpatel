'''
https://en.wikipedia.org/wiki/Right-hand_rule
Panda3D uses a right-handed coordinate system, with the X axis pointing to the right, the Y axis pointing forward, and the Z axis pointing up.
Resonance Audio usese a left-handed coordinate system, with the X axis pointing forward, the Y axis pointing to the left, and the Z axis pointing up.
'''

'''
x - front
y - left
z - up
'''
from geom_models.card import Card
from geom_models.axis import Axis
from geom_models.grid import Grid

def drawCoordinateSystem(dist, spacing, renderer):
    cx = Card('x')
    cx.setPos(dist, 0, 0)
    cx.reparentTo(renderer)

    cy = Card('y')
    cy.setPos(0, dist, 0)
    cy.reparentTo(renderer)

    cz = Card('z')
    cz.setPos(0 ,0, dist)
    cz.reparentTo(renderer)

    g = Grid(10, spacing)
    g.reparentTo(renderer)

    ax = Axis(10)
    ax.reparentTo(renderer)
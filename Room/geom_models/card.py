from panda3d.core import TextNode, NodePath

class Card(NodePath):
    def __init__(self, cardText) -> None:
        text = TextNode(cardText)
        text.setText(cardText)
        text.setFrameColor(0, 0, 1, 1)
        text.setTextColor(1, 1, 1, 1)
        text.setTextScale(0.5)

        NodePath.__init__(self, text.generate())

'''
def makeUpdatableCard(cardText):
    text = TextNode(cardText)
    text.setText(cardText)
    text.setFrameColor(0, 0, 1, 1)
    text.setFrameAsMargin(0.2, 0.2, 0.1, 0.1)
    node = NodePath(text.generate())
    node.text2 = text
    return node
'''
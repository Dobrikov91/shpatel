from panda3d.core import NodePath, Geom, GeomVertexFormat, GeomVertexData, GeomVertexWriter, GeomTriangles, GeomNode, Vec3, Vec4
import math

class Tetrahedron(NodePath):
    def __init__(self, size):
        # Define the vertex format
        format = GeomVertexFormat.getV3cp()

        # Create a GeomVertexData object
        vdata = GeomVertexData('tetra_data', format, Geom.UHStatic)

        # Create a GeomTriangles object
        tetra = GeomTriangles(Geom.UHStatic)

        # Create a GeomVertexWriter to write to the vertex data
        vertex_writer = GeomVertexWriter(vdata, 'vertex')
        color_writer = GeomVertexWriter(vdata, 'color')

        # Define the tetrahedron vertices
        vertices = [
            Vec3(0, 1, 0) * size,
            Vec3(math.sqrt(2)/2, -math.sqrt(2)/2, 0) * size,
            Vec3(-math.sqrt(2)/2, -math.sqrt(2)/2, 0) * size,
            Vec3(0, -math.sqrt(2)/2, 0.5) * size,
        ]


        # Define the tetrahedron faces
        faces = [
            (0, 1, 2),
            (0, 2, 3),
            (0, 3, 1),
            (1, 3, 2)
        ]

        # Define the edge colors (in RGBA format)
        edge_colors = [
            Vec4(1, 0, 0, 1),  # Red
            Vec4(0, 1, 0, 1),  # Green
            Vec4(0, 1, 0, 1),  # Green
            Vec4(1, 1, 0, 1),  # Yellow
            Vec4(0, 1, 0, 1),  # Green
            Vec4(0, 1, 0, 1),  # Green
        ]

        # Add vertices and colors to the vertex data
        for i, vertex in enumerate(vertices):
            vertex_writer.addData3f(vertex)
            color_writer.addData4f(edge_colors[i])

        # Add faces to the GeomTriangles
        for face in faces:
            tetra.addVertices(face[0], face[1], face[2])
            tetra.closePrimitive()

        # Create a Geom object to contain the tetrahedron
        geom = Geom(vdata)
        geom.addPrimitive(tetra)

        # Create a GeomNode to attach the Geom to
        geom_node = GeomNode('tetra')
        geom_node.addGeom(geom)

        NodePath.__init__(self, geom_node)
